from django.contrib import admin
from .models import Email, AllowedEmailSender
from email_manager.tasks import send_async_email

def retry_email(modeladmin, request, queryset):
    for email in queryset:
        if email.status == 'error':
            email.status = 'queued'
            email.retry_count += 1  # Increment retry_count if you added this field
            email.save()
            if email.scheduled_time:
                send_async_email.apply_async(
                    (email.id,), eta=email.scheduled_time)

            if not email.scheduled_time:
                send_async_email.delay(email.id)


retry_email.short_description = "Retry selected emails"

@admin.register(Email)
class EmailAdmin(admin.ModelAdmin):
    list_display = ('id', 'subject', 'to_email', 'status', 'from_email', 'sender', 'queued_at', 'sent_at', 'retry_count')
    list_filter = ('status', 'sender')
    search_fields = ('subject', 'to_email')
    actions = [retry_email]

@admin.register(AllowedEmailSender)
class AllowedEmailSenderAdmin(admin.ModelAdmin):
    list_display = ('user', 'from_email')
    list_filter = ('user',)
    search_fields = ('user__username', 'from_email')
