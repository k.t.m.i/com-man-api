from rest_framework import serializers
from .models import Email

class EmailSerializer(serializers.ModelSerializer):
    sender = serializers.ReadOnlyField(source='sender.username')

    class Meta:
        model = Email
        fields = '__all__'


class EmailCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Email
        fields = ['subject', 'body', 'from_email', 'from_name', 'to_email', 'scheduled_time']
