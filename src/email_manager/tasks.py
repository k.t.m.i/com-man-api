from celery import shared_task
from django.utils import timezone
from .models import Email
from django.core.mail import send_mail

@shared_task
def send_async_email(email_id):
    email = Email.objects.get(pk=email_id)

    if email.from_name:
      from_field = f"{email.from_name} <{email.from_email}>"
    else:
      from_field = email.from_email

    try:
        send_mail(
            subject=email.subject,
            message=email.body,
            html_message=email.body,
            from_email=from_field,
            recipient_list=[email.to_email],
            fail_silently=False
        )
        email.sent_at = timezone.now()
        email.status = 'sent'
    except Exception as e:
        email.status = 'error'
        email.error_message = str(e)
    finally:
        email.save()
