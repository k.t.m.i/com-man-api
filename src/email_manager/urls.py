from django.urls import path, include

from rest_framework.routers import DefaultRouter

from .views import DecoratedTokenObtainPairView, DecoratedTokenRefreshView, EmailViewSet

router = DefaultRouter()
router.register(r'v1/emails', EmailViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('v1/token/', DecoratedTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('v1/token/refresh/', DecoratedTokenRefreshView.as_view(), name='token_refresh'),
]
